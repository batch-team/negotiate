package negotiate

import (
	"bufio"
	"context"
	"crypto/tls"
	"fmt"
	"github.com/jcmturner/gokrb5/v8/client"
	"github.com/jcmturner/gokrb5/v8/config"
	"github.com/jcmturner/gokrb5/v8/credentials"
	"github.com/jcmturner/gokrb5/v8/spnego"
	"io"
	"log"
	"net"
	"net/http"
	"net/url"
	"os"
	"strings"
)

var logger *log.Logger

type Options struct {
	HttpClient         *http.Client
	Logger             *log.Logger
	Context            context.Context
	NoAliasDereference bool
}

type Option func(*Options)

func WithHttpClient(client *http.Client) Option {
	return func(options *Options) {
		options.HttpClient = client
	}
}

// NoAliasDeref : Indicates that the given URI alias should not be de-referenced and should be used as-is.
//
//	This option should be used in distributed environments like Kubernetes.
func NoAliasDeref() Option {
	return func(options *Options) {
		options.NoAliasDereference = true
	}
}

func WithLogger(logger *log.Logger) Option {
	return func(options *Options) {
		options.Logger = logger
	}
}

func WithContext(context context.Context) Option {
	return func(options *Options) {
		options.Context = context
	}
}

// KerberosHTTPRequest Negotiate adds a negotiated Authorization header to a net/http Request object
func KerberosHTTPRequest(method, address string, body io.ReadCloser, opts ...Option) (*http.Response, error) {
	options := new(Options)
	for _, opt := range opts {
		opt(options)
	}
	// Default values
	if options.Logger == nil {
		options.Logger = log.New(io.Discard, "", 0)
	}
	logger = options.Logger

	if options.HttpClient == nil {
		setDefaultHttpClientTransport()
	}

	if options.Context == nil {
		options.Context = context.Background()
	}

	logger.Println("Attempting to fetch the Kerberos config...")
	krb5Client, err := fetchDefaultKerberosClient()
	logger.Println("Attempting to initialize the Kerberos client...")
	kerbSpCl := spnego.NewClient(krb5Client, options.HttpClient, "")

	logger.Printf("Resolving hostname for the given address [%s]...\n", address)
	aliasAddress := address
	if !options.NoAliasDereference {
		resolvedPath, err := resolveAddress(address)
		if err != nil {
			return nil, err
		}
		aliasAddress = resolvedPath
	}

	logger.Printf("Creating an Kerberos authenticated HTTP request. [%s:%s]\n", method, aliasAddress)
	request, err := http.NewRequest(method, aliasAddress, body)
	if err != nil {
		return nil, err
	}

	logger.Println("Executing the request...")
	request = request.WithContext(options.Context)
	return kerbSpCl.Do(request)
}

func KerberosHTTPRequestAsync(method, address string, body io.ReadCloser, result chan interface{}, opts ...Option) {
	go func() {
		res, err := KerberosHTTPRequest(method, address, body, opts...)
		if err != nil {
			result <- err
			return
		}
		result <- res
	}()
}

func fetchDefaultKerberosClient() (*client.Client, error) {
	kCfg, err := fetchFilteredKrb5Config()
	if err != nil {
		return nil, err
	}

	var cCname string
	var ok bool
	cCname, ok = os.LookupEnv("KRB5CCNAME")
	if !ok {
		cCname = defaultKrbCCName()
	}
	shortCCname := strings.TrimPrefix(cCname, "FILE:")

	logger.Printf("Retrieved the Kerberos cache file name [%s]\n", shortCCname)
	cCache, err := credentials.LoadCCache(shortCCname)
	logger.Println("Creating a new client from cache...")
	cl, err := client.NewFromCCache(cCache, kCfg)
	if err != nil {
		if strings.HasPrefix(err.Error(), "TGT") {
			return nil, fmt.Errorf("err: %v, no valid kerberos token found", err)
		}
		return nil, err
	}
	return cl, nil
}

func fetchFilteredKrb5Config() (c *config.Config, err error) {
	cfgFName := getKrbConfFileName()
	logger.Printf("Attempting to read the Kerberos configuration file [%s]...\n", cfgFName)
	fh, err := os.Open(cfgFName)
	if err != nil {
		return nil, err
	}
	defer func() {
		err = fh.Close()
	}()

	c, err = config.NewFromScanner(getCERNKrb5Scanner(fh))
	if err != nil {
		return nil, err
	}
	return
}

// Return a scanner which tastefully omits the "v4_name_convert"
// subsection from CERN's krb5.conf, which chokes gokrb5.
func getCERNKrb5Scanner(fh *os.File) *bufio.Scanner {
	scanner := bufio.NewScanner(fh)
	filter := 0
	split := func(data []byte, atEOF bool) (advance int, token []byte, err error) {
		advance, token, err = bufio.ScanLines(data, atEOF)
		// drop the first line containing the offending string
		if strings.Contains(string(token), "v4_name_convert") {
			filter++
			token = []byte("")
			return
		}
		if filter > 0 {
			if strings.Contains(string(token), "{") {
				filter++
			}
			if strings.Contains(string(token), "}") {
				filter--
			}
			token = []byte("")
			return
		}
		return
	}
	scanner.Split(split)

	return scanner
}

func resolveAddress(address string) (resolvedAddress string, err error) {
	parsedUrl, err := url.ParseRequestURI(address)
	if err != nil {
		return resolvedAddress, err
	}

	entries, err := net.LookupHost(parsedUrl.Hostname())
	if err != nil || len(entries) < 1 {
		return resolvedAddress, err
	}

	firstResolvedIP := entries[0]
	logger.Printf("Resolved IP entry [%s]...\n", firstResolvedIP)
	reversedHosts, err := net.LookupAddr(firstResolvedIP)
	if err != nil {
		return resolvedAddress, err
	}

	firstReversedHost := strings.TrimRight(reversedHosts[0], ".")
	logger.Printf("Resolved hostname [%s]...\n", firstReversedHost)
	return fmt.Sprintf("%s://%s:%s%s", parsedUrl.Scheme, firstReversedHost, parsedUrl.Port(), parsedUrl.Path), nil
}

func defaultKrbCCName() string {
	krbCCName := fmt.Sprintf("FILE:/tmp/krb5cc_%d", os.Getuid())
	systemdKrbCCPath := fmt.Sprintf("/var/run/user/%d/krb5cc", os.Getuid())
	if _, err := os.Stat(systemdKrbCCPath); err == nil {
		krbCCName = fmt.Sprintf("FILE:%s", systemdKrbCCPath)
	}
	logger.Printf("Fetching the default Kerberos CCNAME [%s]...\n", krbCCName)
	return krbCCName
}

func setDefaultHttpClientTransport() {
	logger.Println("Setting the default HTTP transport to skip insecure certificates...")
	http.DefaultTransport.(*http.Transport).TLSClientConfig = &tls.Config{InsecureSkipVerify: true}
}

func getKrbConfFileName() (kfile string) {
	if value, ok := os.LookupEnv("KRB5_CONFIG"); ok {
		return value
	}
	return "/etc/krb5.conf"
}
