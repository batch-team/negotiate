package tests

import (
	"gitlab.cern.ch/batch-team/negotiate"
	"log"
	"net/http"
	"os"
	"testing"
)

var address string
func init() {
	address = "https://haggis.cern.ch:8204/accounting"
}

func TestHttpMethodSync(t *testing.T) {
	res, err := negotiate.KerberosHTTPRequest(http.MethodGet, address, nil, negotiate.WithLogger(log.New(os.Stdout, "", log.LstdFlags)))
	if err != nil {
		t.Fatal(err)
	}

	if res.StatusCode != http.StatusOK {
		t.Fatalf("expected status code [200] but got [%d] instead", res.StatusCode)
	}
}

func BenchmarkHttpMethodSync(b *testing.B) {
	for i := 0; i < b.N; i++ {
		res, err := negotiate.KerberosHTTPRequest(http.MethodGet, address, nil)
		if err != nil {
			b.Fatal(err)
		} else if res.StatusCode != http.StatusOK {
			b.Fatalf("expected status code [200] but got [%d] instead", res.StatusCode)
		}
	}
}